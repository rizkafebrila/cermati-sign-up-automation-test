import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.cermati.com/gabung')

WebUI.setText(findTestObject('Sign-Up_Objects/Page_Bandingkan Kartu Kredit Pinjam/input__email'), 'rizka_febrilas@mailinator.com')

WebUI.setEncryptedText(findTestObject('Sign-Up_Objects/Page_Bandingkan Kartu Kredit Pinjam/input__password'), 'to8YNdH2rfQpoqlskQvTuA==')

WebUI.setText(findTestObject('Sign-Up_Objects/Page_Bandingkan Kartu Kredit Pinjam/input__firstName'), 'Rizka')

WebUI.setText(findTestObject('Sign-Up_Objects/Page_Bandingkan Kartu Kredit Pinjam/input__lastName'), 'FS')

WebUI.setText(findTestObject('Sign-Up_Objects/Page_Bandingkan Kartu Kredit Pinjam/input__mobilePhone'), '081255672966')

WebUI.setText(findTestObject('Sign-Up_Objects/Page_Bandingkan Kartu Kredit Pinjam/input__residenceCity'), 'Jakarta')

WebUI.click(findTestObject('Sign-Up_Objects/Page_Bandingkan Kartu Kredit Pinjam/div_KOTA JAKARTA BARAT'))

WebUI.setText(findTestObject('Sign-Up_Objects/Page_Bandingkan Kartu Kredit Pinjam/input__residenceCity'), 'KOTA JAKARTA BARAT')

WebUI.click(findTestObject('Sign-Up_Objects/Page_Bandingkan Kartu Kredit Pinjam/button_Daftar Akun'))

