import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.cermati.com/gabung')

WebUI.setText(findTestObject('Object Repository/Sign-Up_Objects/Page_Bandingkan Kartu Kredit Pinjam/input__email'), 'rizksfs@')

WebUI.click(findTestObject('Object Repository/Sign-Up_Objects/Page_Bandingkan Kartu Kredit Pinjam/div_Masuk ke Cermati.com'))

WebUI.setText(findTestObject('Object Repository/Sign-Up_Objects/Page_Bandingkan Kartu Kredit Pinjam/input__email'), 'rizkafs@mailinator.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Sign-Up_Objects/Page_Bandingkan Kartu Kredit Pinjam/input__password'), 
    'to8YNdH2rfQpoqlskQvTuA==')

WebUI.setText(findTestObject('Object Repository/Sign-Up_Objects/Page_Bandingkan Kartu Kredit Pinjam/input__firstName'), 
    'Rizka ')

WebUI.setText(findTestObject('Object Repository/Sign-Up_Objects/Page_Bandingkan Kartu Kredit Pinjam/input__lastName'), 'FS')

WebUI.setText(findTestObject('Object Repository/Sign-Up_Objects/Page_Bandingkan Kartu Kredit Pinjam/input__mobilePhone'), 
    '0822002200998')

WebUI.setText(findTestObject('Object Repository/Sign-Up_Objects/Page_Bandingkan Kartu Kredit Pinjam/input__residenceCity'), 
    'Jakarta')

WebUI.click(findTestObject('Object Repository/Sign-Up_Objects/Page_Bandingkan Kartu Kredit Pinjam/div_KOTA JAKARTA BARAT_1'))

WebUI.setText(findTestObject('Object Repository/Sign-Up_Objects/Page_Bandingkan Kartu Kredit Pinjam/input__residenceCity'), 
    'KOTA JAKARTA BARAT')

WebUI.click(findTestObject('Object Repository/Sign-Up_Objects/Page_Bandingkan Kartu Kredit Pinjam/button_Daftar Akun'))

